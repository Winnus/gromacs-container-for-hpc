#!/usr/bin/python3
Stage0 += baseimage(image={{GROMACS_IMAGE_NAME}}, _distro='ubuntu22')
gcc_version = USERARG.get('gcc_version', {{GCC_VERSION}})

# clean up:
# clean gcc and clean env 
Stage0 += shell(commands=
        [
            'spack uninstall -y {}'.format(gcc_version),
            'spack gc -y',
            '. /load-spack-env.sh',
            'spack gc -y'
        ])
