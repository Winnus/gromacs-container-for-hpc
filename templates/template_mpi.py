#!/usr/bin/python3
Stage0 += baseimage(image={{BASE_IMAGE_NAME}}, _distro='ubuntu22')

# software
gcc_version = USERARG.get('gcc_version', {{GCC_VERSION}})
mpi_version = USERARG.get('mpi_version', {{MPI_VERSION}})
fftw_version = USERARG.get('fftw_version', {{FFTW_VERSION}})

# spack install
Stage0 += shell(commands=
        [       
            '. /load-spack-env.sh',
            'spack env deactivate',
            'spack install {}'.format(gcc_version),
            'spack compiler add $(spack location -i {})'.format(gcc_version),
        ])
Stage0 += shell(commands=
        [       
            '. /load-spack-env.sh',
            'spack add {}'.format(mpi_version),
            'spack add {}'.format(fftw_version),
            'spack install',
            'spack clean --all'
        ])
        
        


