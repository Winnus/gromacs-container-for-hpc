#!/usr/bin/python3
from hpccm.templates.git import git
Stage0 += baseimage(image={{ROOT_IMAGE_NAME}}, _distro="ubuntu22")

# Spack dependencies and Python (build-in python does not work with ubuntu 22)
ospackages = ['build-essential', 'make', 'patch', 'bash', 'tar', 'gzip', 'unzip', 'bzip2', 'xz-utils',
              'zstd', 'file', 'gnupg2', 'git', 'python3-dev', 'curl', 'ca-certificates', 'autoconf',
              'vim', 'pkg-config', 'gfortran', 'python2', 'python3']
Stage0 += apt_get(ospackages=ospackages)


# Setup and install Spack
spack_version = USERARG.get('spack_version', 'releases/{{SPACK_VERSION}}')
Stage0 += shell(commands=[
    git().clone_step(repository='https://github.com/spack/spack', branch=spack_version, path='/opt'),
    'ln -s /opt/spack/share/spack/setup-env.sh /etc/profile.d/spack.sh',
    'ln -s /opt/spack/share/spack/spack-completion.bash /etc/profile.d'
])

if {{USER_CONCRETIZATION}}:
    Stage0 += copy(src='spack-packages.yaml', dest='/opt/spack/etc/spack/packages.yaml')
Stage0 += copy(src='spack-environment.yaml', dest='/opt/spack-env/spack.yaml')

Stage0 += environment(variables={'PATH': '/opt/spack/bin:/opt/view:$PATH', 'SPACK_ROOT': '/opt/spack'})

Stage0 += shell(commands=[
    'echo ". /opt/spack/share/spack/setup-env.sh" >> /load-spack-env.sh',
    'echo "spack env activate /opt/spack-env" >> /load-spack-env.sh'
])

