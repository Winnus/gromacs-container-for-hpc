#!/usr/bin/python3
# -------------------------------------------
# clean up
# -------------------------------------------
Stage0 += baseimage(image={{GROMACS_IMAGE_NAME}}, _distro='ubuntu22', _as='base')

gcc_version = USERARG.get('gcc_version', {{GCC_VERSION}})

# clean up:
# clean gcc and clean env 
Stage0 += shell(commands=
        [
            'spack uninstall -y {}'.format(gcc_version),
            'spack gc -y',
            '. /load-spack-env.sh',
            'spack gc -y'
        ])
        
# -------------------------------------------
# copy spack env to new base
# -------------------------------------------
Stage1 += baseimage(image={{ROOT_IMAGE_NAME}}, _distro="ubuntu22")

# Spack dependencies and Python
ospackages = ['build-essential', 'make', 'patch', 'bash', 'tar', 'gzip',
'unzip', 'bzip2', 'xz-utils', 'zstd', 'file', 'gnupg2', 'git',
'python3-dev', 'curl', 'ca-certificates', 'autoconf', 'vim',
'pkg-config', 'gfortran', 'python2', 'python3'
]
Stage1 += apt_get(ospackages=ospackages)

# Copy Spack
Stage1 += copy(_from='base', src='/opt', dest='/opt')
Stage1 += copy(_from='base', src='/root/.spack', dest='/root/.spack')
Stage1 += shell(commands=[
    'ln -s /opt/spack/share/spack/setup-env.sh /etc/profile.d/spack.sh',
    'ln -s /opt/spack/share/spack/spack-completion.bash /etc/profile.d'
])
Stage1 += environment(variables={'PATH': '/opt/spack/bin:/opt/view:$PATH', 'SPACK_ROOT': '/opt/spack'})
Stage1 += shell(commands=[
    'echo ". /opt/spack/share/spack/setup-env.sh" >> /load-spack-env.sh',
    'echo "spack env activate /opt/spack-env" >> /load-spack-env.sh'
])


