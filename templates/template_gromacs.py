#!/usr/bin/python3
Stage0 += baseimage(image={{MPI_IMAGE_NAME}}, _distro='ubuntu22')

gromacs_version = USERARG.get('gromacs_version', {{GROMACS_VERSION}})
Stage0 += shell(commands=
        [
            '. /load-spack-env.sh',
            'spack add {}'.format(gromacs_version),
            'spack install',
            'spack clean --all'
        ])

