#!/usr/bin/bash
set -euxo pipefail

## -------------------------------
##      Usage
## -------------------------------
## This script will build podman/docker containers
## in which a spack environment will compile
## a specific version of gcc, mpi, fftw and gromacs.
## For each step a new image is generated which will result in
## 1. a base image containing spack
## 2. a mpi image containing gcc, fftw and mpi
## 3. a gromacs image containing gromacs
## 4. a deploy image striped of the compiler
## fftw will be compiled for a specified target architecture.
## The image size will be reduced by removing the compiler
## once all build steps are completed.
## This involves an approach to remove intermediate layers from
## the image as otherwise the removed compiler is present in these
## intermediate layers and the size of the final image will stay the same.
## To install the versions in need modify the variables below.
## You will need to have HPCCM installed:
## https://github.com/NVIDIA/hpc-container-maker

## -------------------------------
##      variables
## -------------------------------
## define builder
BUILDER='podman' ## 'podman' 'docker'

## remove build recipes once build is finished?
REMOVE_RECIPES=false ## 'true' 'false'

## define how to deploy the final image
## squash: builder removes all layers in final build
## copy: spack env will be copied to new, clean image
FINAL_IMAGE_DEPLOY_METHOD='squash' ## 'squash' 'copy'

## define the build architecture
ARCH='cascadelake' ## 'cascadelake' 'ivybridge' 'hashwell' ...

## image names
ROOT_IMAGE_NAME='docker.io/library/ubuntu:22.04'
BASE_IMAGE_NAME="gwdg/hpc-base-ubuntu-$ARCH:latest"
MPI_IMAGE_NAME="gwdg/hpc-mpi-ubuntu-$ARCH:latest"
GROMACS_IMAGE_NAME="gwdg/hpc-gromacs-ubuntu-$ARCH:latest"
DEPLOY_IMAGE_NAME="gwdg/hpc-gromacs-deploy-ubuntu-$ARCH:latest"

## spack version
SPACK_VERSION='v0.19'

## declare compiler, mpi and fftw to be installed
## these have to be spack commands
## to get available versions use a spack installation or
## use their website:
## "https://spack.readthedocs.io/en/latest/package_list.html"
GCC_VERSION='gcc@12.2.0 target=x86_64'
MPI_VERSION='openmpi@4.1.4' ## do not provide GCC version (added automatically)
FFTW_VERSION='fftw' ## do not provide the architecture (target=xyz) here
GROMACS_VERSION='gromacs@2022.3'

## use user concretization preferences for Spack?
## if true modifie the file spack-packages.yaml according to your needs
## you will need that for example to build openmpi with slurm or fabric support!
## or you need to add all that to the version above but... that might be confusing
USER_CONCRETIZATION=true





## -------------------------------
##      modify variables
## -------------------------------
## mpi will need the compiler version that were specified with GCC_VERSION
## gromacs and fftw will compile using the same compiler even not
## specifically defined (spack knows)
MPI_VERSION=$MPI_VERSION"%$GCC_VERSION"

## add the architecture to fftw command
## since gromacs rely on fftw spack will
## compile gromacs with respect to $ARCH as well
FFTW_VERSION_ARCH=$FFTW_VERSION" target=$ARCH" 



## -------------------------------
##      cook recipes
## -------------------------------
## 
## 1. make hppcm recipes
## ----------------------
##
## spack base image
cp templates/template_base.py base.py
if [ $USER_CONCRETIZATION = true ]; then
	sed -i "s|{{USER_CONCRETIZATION}}|True|" base.py
else
	sed -i "s|{{USER_CONCRETIZATION}}|False|" base.py
fi

sed -i "s|{{ROOT_IMAGE_NAME}}|'$ROOT_IMAGE_NAME'|" base.py ## set the root image
sed -i "s|{{SPACK_VERSION}}|$SPACK_VERSION|" base.py

## compiler mpi, fftw image
cp templates/template_mpi.py mpi.py
sed -i "s|{{BASE_IMAGE_NAME}}|'$BASE_IMAGE_NAME'|" mpi.py ## set the base image
sed -i "s|{{GCC_VERSION}}|'$GCC_VERSION'|" mpi.py ## set the compiler to be used
sed -i "s|{{MPI_VERSION}}|'$MPI_VERSION'|" mpi.py ## set the mpi build
sed -i "s|{{FFTW_VERSION}}|'$FFTW_VERSION_ARCH'|" mpi.py ## set the fftw version

## gromacs image
cp templates/template_gromacs.py gromacs.py
sed -i "s|{{MPI_IMAGE_NAME}}|'$MPI_IMAGE_NAME'|" gromacs.py ## set the base image
sed -i "s|{{GROMACS_VERSION}}|'$GROMACS_VERSION'|" gromacs.py ## set the gromacs version

## deploy image (copy)
cp templates/template_deploy_copy.py deploy_copy.py
sed -i "s|{{ROOT_IMAGE_NAME}}|'$ROOT_IMAGE_NAME'|" deploy_copy.py ## set the root image for final copy container
sed -i "s|{{GROMACS_IMAGE_NAME}}|'$GROMACS_IMAGE_NAME'|" deploy_copy.py ## set the base image
sed -i "s|{{GCC_VERSION}}|'$GCC_VERSION'|" deploy_copy.py ## set the compiler to be used

## deploy image (squash)
cp templates/template_deploy_squash.py deploy_squash.py
sed -i "s|{{GROMACS_IMAGE_NAME}}|'$GROMACS_IMAGE_NAME'|" deploy_squash.py ## set the base image
sed -i "s|{{GCC_VERSION}}|'$GCC_VERSION'|" deploy_squash.py ## set the compiler to be used

##
## 
## 2. make docker definition files
## -------------------------------
## Base
hpccm --recipe base.py > Dockerfile.base

## MPI
hpccm --recipe mpi.py > Dockerfile.mpi

## Gromacs
hpccm --recipe gromacs.py > Dockerfile.gromacs

## deploy
hpccm --recipe deploy_copy.py > Dockerfile.deploy_copy
hpccm --recipe deploy_squash.py > Dockerfile.deploy_squash


## -------------------------------
##      build containers
## -------------------------------
## Base
$BUILDER build --no-cache -t $BASE_IMAGE_NAME -f Dockerfile.base .

## MPI
$BUILDER build --no-cache -t $MPI_IMAGE_NAME -f Dockerfile.mpi .

## Gromacs
$BUILDER build --no-cache -t $GROMACS_IMAGE_NAME -f Dockerfile.gromacs .

## deploy
## copy deploy
if [ $FINAL_IMAGE_DEPLOY_METHOD = 'copy' ]; then
	$BUILDER build --no-cache -t $DEPLOY_IMAGE_NAME -f Dockerfile.deploy_copy .
fi

## squash deploy 
if [ $FINAL_IMAGE_DEPLOY_METHOD = 'squash' ]; then
	if [ $BUILDER = 'podman' ]; then
		$BUILDER build --no-cache --squash-all -t $DEPLOY_IMAGE_NAME -f Dockerfile.deploy_squash .
	fi
	if [ $BUILDER = 'docker' ]; then
		$BUILDER build --no-cache --squash -t $DEPLOY_IMAGE_NAME -f Dockerfile.deploy_squash .
	fi
fi


## -------------------------------
##      clean up
## -------------------------------
## remove recipes
if [ $REMOVE_RECIPES = true ]; then
	rm -f -- base.py
	rm -f -- mpi.py
	rm -f -- gromacs.py
	rm -f -- deploy_copy.py
	rm -f -- deploy_squash.py
	rm -f -- Dockerfile.base
	rm -f -- Dockerfile.mpi
	rm -f -- Dockerfile.gromacs
	rm -f -- Dockerfile.deploy_copy
	rm -f -- Dockerfile.deploy_squash
fi
