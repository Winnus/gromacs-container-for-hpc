This repository holds the code to easily build a reasonable sized GROMACS cotainer image with MPI support.
Spack (https://github.com/spack/spack) is utilized to build all the necessary software and optimize it for a specific target architecture. The initial idea of using multiple images and the HPCCM recipe files where provided by Azat Khuziyakhmetov and are modified. All relevant build informations (software versions, image names, ...) should be given in the ```build.sh``` script.
The build script will build multiple images which are
1. base image with spack installed
2. spack install GCC, MPI, FFTW
3. spack install GROMACS
4. resize the image by remove GCC and unused deps

To build the image follow these steps:
* install HPC Container Maker (https://github.com/NVIDIA/hpc-container-maker)
* modifie the variables in the ```build.sh``` script to your needs
* run the script and hope for no build errors
* copy the final image to the HPC system as described below




# run the image on a HPC cloud
To run the image on a HPC cloud it has to be
1. exported as a tar archive
2. copied to the cloud
3. imported into singularity
4. runned locally on the login node or better with slurm

## howto

Save the image on the local workstation into a tar archive where ```gwdg/hpc-gromacs-deploy-ubuntu-cascadelake:latest``` should be replaced with the proper build name of the image.
```
podman save --output deploy_gromacs.tar gwdg/hpc-deploy-ubuntu-cascadelake:latest
```

\
Copy the file to the HPC system, for example use sshfs to mount the cloud to a global dir and copy it
```
mkdir ~/cloud_gwdg
sshfs user@login-mdc.hpc.gwdg.de: ~/cloud_gwdg/ -o IdentityFile=~/keyfile
cp deploy_gromacs.tar /cloud_gwdg/deploy_gromacs.tar
```

\
Log into the HPC system and load singularity
```
module load singularity
```

\
Use singularity to build the container into a directory/sandbox format.
Here deploy_gromacs is the name that singularity will use as its container name (the container directory) and docker-archive://deploy_gromacs.tar will tell that a local docker archive named deploy_gromacs.tar should be used do build the container
```
singularity build --sandbox deploy_gromacs docker-archive://deploy_gromacs.tar
```


### prepare a simulation
In order to run a simulation we first create a folder for the gromacs simulation files
```
mkdir gromacs-sim
cd gromacs-sim
```

\
were going to use the benchMEM from https://www.mpinat.mpg.de/grubmueller/bench
```
wget https://www.mpinat.mpg.de/benchMEM
# then unzip it and back to home
unzip benchMEM
cd ../
```

### local run
you can run the installed gromacs on the login node by shell into the singularity container
```
singularity shell deploy_gromacs

# now change into the dir where the container files are
cd deploy_gromacs

# and source the spack env file to get access to the spack installed sw 
. /load-spack-env.sh

# now run the bench simulation
cd ../gromacs-sim
mpirun -np 4 gmx_mpi mdrun -s benchMEM.tpr -nsteps 400 -resetstep 200
```


### slurm
You wanna use the compute nodes and not the login one, but they need to be accessed via slurm. This requires a little bit more set-up. We will need a jobscript that runs singularity and tells it to start our simulation within the container. Create a file with vim in home ~
```
cd ~
vim slurm_singularity_gromacs.sh 
```
copy this into the file (and maybe adjust the slurm parameters)
```
#!/bin/bash
#SBATCH --job-name=gromacs_sim
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4G
#SBATCH --partition=medium
#SBATCH -o %x-%j.out # File to which STDOUT will be written
#SBATCH -e %x-%j.err # File to which STDERR will be written

singularity run deploy_gromacs "gromacs-sim/run_sim.sh"
```

\
then make a file named ```run_sim.sh``` in the folder ```gromacs-sim``` which will be called from the container process to start the simulation. 
```
# cd to the dir
cd gromacs-sim

# make the file
vim run_sim.sh
```
put this content into it. It will load the spack env inside the container and then runs the simulation we downloaded into ```~/gromacs-sim/benchMEM.tpr```
```
#!/usr/bin/bash
. /load-spack-env.sh
exec gmx_mpi mdrun -s gromacs-sim/benchMEM.tpr -nsteps 400 -resetstep 200
```
allow execution of the script
```
chmod +x run_sim.sh
```

run the simulation via slurm sbatch
```
cd ~
sbatch slurm_singularity_gromacs.sh     
```



### errors with read only container file systems
For some reason the mpi process might want to write to ```/local```. As the container dirs are read only it will fail. To overcome this bind a writable dir into the ```/local``` dir of the container.
```
cd ~
mkdir local_write
# set permissions to global welcome (this is not so good maybe)
chmod -R 777 local_write
```

Modifie the sbatch script ```slurm_singularity_gromacs.sh``` to let singularity mount the directory ```~/local_write``` to ```/local``` inside the container. The file should be look like

```
#!/bin/bash
#SBATCH --job-name=gromacs_sim
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8G
#SBATCH --partition=medium
#SBATCH -o %x-%j.out # File to which STDOUT will be written
#SBATCH -e %x-%j.err # File to which STDERR will be written

singularity run --bind local_write:/local deploy_gromacs "gromacs-sim/run_sim.sh"
```

now simulations hopefully run!